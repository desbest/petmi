<?php include ("config.php"); ?>
<html>
<head>
<title>Petmi</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<meta name="viewport" content="width=device-width">

<script type="text/javascript" src="jquery2.1.4-min.js"></script>

<script type="text/javascript">
$(function () {

	$("form").on("submit", function (e) {
		
        //e.preventDefault(); // prevent form from submitting
        //Submit form ajax
        //$('input:button').attr("disabled", true);
		$('input:submit').attr("disabled", true);
		$('input:submit').fadeOut(300);
		$("#loadingblock").fadeIn(300);
        
    });
});
</script>

</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Step 1</a></li>
<li><a>Step 2</a></li>
<li><a>Step 3</a></li>
<li id="active"><a id="current">Step 4</a></li>
<li><a>Step 5</a></li>
<li><a>Step 6</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Petmi v<?=$softwareversion;?></font><br>
<?php

//gets the config page
if (isset($_POST['lookforthis']) && $_POST['lookforthis'] == "ichosesplitting")
{

			$folder = "splits/";
			
			$fh = fopen("importme.sql", 'a') or die("can't open file");
			$stringData = "-- --------------------------------------------------------";
			fwrite($fh, $stringData);
			fclose($fh);
			
			
			$file2 = fopen("importme.sql","r");
			
			//echo "<br><textarea class=\"mediumtext\" style=\"width: 500px; height: 200px;\">";
			$danumber = "1";
			while(! feof($file2)){
				//echo fgets($file2)."<!-- <br /><hr color=\"red\" size=\"15\"> -->"; 
				$oneline = fgets($file2); //this is fgets($file2) but formatted nicely
				//echo "<br>$oneline";
				
				$findme1  = '-- --------------------------------------------------------';
				$pos1 = strpos($oneline, $findme1);
				$findme2  = '-- Table structure for';
				$pos2 = strpos($oneline, $findme2);
				$findme3  = '-- Dumping data for';
				$pos3 = strpos($oneline, $findme3);
				$findme4  = '-- Indexes for dumped tables';
				$pos4 = strpos($oneline, $findme4);
				$findme5  = '-- AUTO_INCREMENT for dumped tables';
				$pos5 = strpos($oneline, $findme5);
				if ($pos1 === false && $pos2 === false && $pos3 === false && $pos4 === false && $pos5 === false) {

					// setcookie("filenumber",$i);
					// if ($danumber2 == ""){$danumber2 = "0";} else { $danumber2 = $danumber2 +1;}					
					$ourFileName = "splits/sql-split-$danumber.sql";
					// echo "writing danumber is $danumber";
					$ourFileHandle = fopen($ourFileName, 'a') or die("can't edit file. chmod directory to 777");

					$stringData = $oneline;
					$stringData = preg_replace("/\/[*][!\d\sA-Za-z@_='+:,]*[*][\/][;]/", "", $stringData);
					$stringData = preg_replace("/\/[*][!]*[\d A-Za-z`]*[*]\/[;]/", "", $stringData);
					$stringData = preg_replace("/DROP TABLE IF EXISTS `[a-zA-Z]*`;/", "", $stringData);
					$stringData = preg_replace("/LOCK TABLES `[a-zA-Z` ;]*/", "", $stringData);
					$stringData = preg_replace("/UNLOCK TABLES;/", "", $stringData);

					fwrite($ourFileHandle, $stringData);
					fclose($ourFileHandle);
				
				} else {
						//write new file;
						if ($danumber == ""){$danumber = "1";} else { $danumber = $danumber +1;}
						$ourFileName = "splits/sql-split-$danumber.sql"; 
						//echo "$ourFileName has been written with the contents above.\n";
						
						$ourFileName = "splits/sql-split-$danumber.sql";
						$ourFileHandle = fopen($ourFileName, 'a') or die("can't edit file. chmod directory to 777");
						$stringData = "$oneline";
						fwrite($ourFileHandle, $stringData);
						fclose($ourFileHandle);
				}
			}
			//echo "</textarea>";
			
		
		fclose($file2);
		 



echo ("
<br><br>
<img src=\"tick.png\">importme.sql has been split
<br><a href=\"step5.php\">Continue Importing</a>
");	
//echo 'done (', count($sql), ' queries).';

		
				
} else{

function is_empty_dir($dir){
if (($files = @scandir($dir)) && count($files) <= 1) {
return true;
}
return false; 
}

// the form has not been submitted...so now we display it.
$folder = "splits/";
if (is_empty_dir("$folder") === true) {
	echo "<img src=\"warning.png\" align=\"left\">The folder splits/ is not empty.
	<br>Delete its contents before continuing with Petmi
	<br><a href=\"step5.php\">Try again</a>
	";
} else {
	echo ("
	<form method=\"POST\" >
	<br>Now it's time to upload your phpmyadmin mysql export dump to your server.
	<br>Make sure that <b>importme.sql</b> is the sql file you want to import to the <b>$database</b> database.
	<br>
	<br>Click the button below.
	<br>This step will create various .sql (text) files in the splits/ directory for each database table you have.
	<br>That way you can import your database table by table.
	<br><br>
	<input type=\"hidden\" name=\"lookforthis\" value=\"ichosesplitting\" />
	<input name=\"makeintosplits\" type=\"submit\" id=\"makeintosplits\" class=\"bigbutton\" value=\"Split .sql file\">
	<div id=\"loadingblock\">
	<img src=\"loading.gif\">
	</div>
	</form>
	"); 
}



}	 

?>			  
			  



</div>			  
</body>
</html>