<?php include ("config.php"); ?>
<html>
<head>
<title>Petmi</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<meta name="viewport" content="width=device-width">
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a >Step 1</a></li>
<li><a >Step 2</a></li>
<li id="active"><a id="current">Step 3</a></li>
<li><a>Step 4</a></li>
<li><a>Step 5</a></li>
<li><a>Step 6</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Petmi v<?=$softwareversion;?></font>
<br>			
			  Edit the config.php file you uploaded.
			  <br>Chmod the splits/ directory to 777 with your ftp program.
			  <br>If done correctly, you can progress to step four.
			  <br><br><a href="step4.php?page=done">I've done that</a>
			  <br>
			<?php
			if (isset($_GET['page']) && $_GET['page'] == "done"){
				if ($conn){ 
				
					$filename = 'importme.sql';
					
					if (!file_exists($filename)) {
					echo "<br><img src=\"warning.png\" align=\"left\">The file $filename does not exist.
					<br>Import the sql file to this directory and call it $filename.
					<br><br><a href=\"step4.php?page=done\">Try again</a>
					
					
					
					";
					} else {
					echo "
					<br>
					<br><img src=\"tick.png\" align=\"left\">You have successfully got a functioning mysql connection.
					<br><img src=\"tick.png\" align=\"left\">The file importme.sql exists.
					"; 
				    } 
				
					if (!is_writable("splits/")) {
					echo "<img src=\"warning.png\" align=\"left\"><br>The directory splits/ is not writable.
					<br>Import the sql file to this directory and call it $filename.
					<br><br><a href=\"step4.php?page=done\">Try again</a>";
					} else {
					echo "
					<br><br>
					<img src=\"tick.png\" align=\"left\">The directory splits/ is writable.
					"; 
				    } 

				   $filename = 'importme.sql';
				   if (is_writable("splits/") && file_exists($filename)){
				   echo "<br><br><a href=\"step5.php\">Continue Importing</a>";
				   } 
				} else {
					echo "
					<img src=\"warning.png\" align=\"left\">
					<br>Petmi failed to connect to a mysql database.
					<br><br><a href=\"step4.php?page=done\">Try again</a>
					";
				}
			}
			?>
			  
			  



</div>			  
</body>
</html