<?php include ("config.php"); ?>
<html>
<head>
<title>Petmi</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<meta name="viewport" content="width=device-width">

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<?php 
$directory = "splits/";
$splitcount = count(glob("" . $directory . "*.sql")); 
?>
<script type="text/javascript">

$(function () {

var splitcount = <?=$splitcount;?>

var jsondump = ""; //define a global
// var jsonstring = "";
//var firstrace = ""; //define a global

if (typeof jsondump !== 'undefined') {
    // the variable is defined
    // alert("defined");
} else {
    // alert("not defined"); 
    jsondump = "";
}

function appendJSON(appendthis, tothis){
    // alert("hello");
    // alert(jsonstring);
    // jsondump += appendthis+jsondump;
    if (tothis == "") { jsondump = tothis+""+appendthis; }
        else { jsondump = tothis+","+appendthis; }
    // alert("jsondump is\n"+jsondump);
    return jsondump;
}


function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}





$("#bulkimportsplits").click(function (){
        startbulkimport();
});

 function startbulkimport() {
      let i;
      let promises = [];
      console.log("there are "+splitcount+" files to import");
      
      $("#bulkimportsplits").fadeOut(600);
     

      for (i = 1; i <= splitcount; i++) {
        // setTimeout(function()  { // don't use setTimeout

            /*
            sleep(300); // 0.3 second delay
            if ((i % 10) == 0 && i != 0){ console.log("Ssh! extra 300ms sleep for every 10 entries"); sleep(300); }
            */

            //0.3 second delay if loop count is multiple of 10
            // some servers have rate limits
            promises.push(doSomethingAsync(i));
        // }, 3000); // 3 second delay
      }
      
      Promise.all(promises)
          .then((results) => {

            successEntries = $.grep(results, function(element, index) {
            return element.includes('success');
            }, false);
            failedCount = splitcount - successEntries.length;
            // https://stackoverflow.com/q/1789945/337306
            // https://www.educba.com/jquery-grep/

            console.log("Operation complete. "+successEntries.length+" tasks was successful and "+failedCount+" failed. See splits/bulkimportlog.json for details", results);
          })
          .catch((e) => {
              // Handle errors here
          });
    }

function doSomethingAsync(ivalue) {
  return new Promise((resolve) => {
    // console.log("#"+ivalue+" making ajax http request for file "+ivalue);
    $('input:button').attr("disabled", true);
    addthisDump = "";
    dataString = "";
    timeoutfor = $('#desiredtimeout').val() * 100;

        $( "#loading" ).fadeIn(600);
        $('input:button').attr("disabled", true);
        // $('input:submit').attr("disabled", true);
        // stop button fron having the capability to be submitted twice
        //console.log("importing file "+ivalue);
        sleep(300);


        dataString = "key=value&key2=value2";
        $.ajax({
            type: "GET",
            url: "restore.php?mode=bulkimport&importthissplit=true&filenumber="+ivalue,
            // url: "index.php",
            // data: dataString,
            //cache: false,
            timeout: timeoutfor, //changeme
            statusCode: {
                404: function() {
                    console.error("Error 404 File Not Found when accessing  restore.php?file="+ivalue);
                    // $("."+dabookie+" td").addClass("yellowback");
                    // $("."+dabookie+" .loading").replaceWith("<img src='cross.png' width='24' height='24' />");
                },
                503: function(){ 
                    // Service Unavailable (server access throttling)
                    // $("."+dabookie+" td").addClass("magentaback");
                    // $("."+dabookie+" .loading").replaceWith("<img src='cross.png' width='24' height='24' />");
                    console.error("Error 504 Gateway Timeout when accessing  restore.php?file="+ivalue);
                },
                504: function(){ // Gateway Timeout
                    // $("."+dabookie+" td").addClass("purpleback");
                    // $("."+dabookie+" .loading").replaceWith("<img src='cross.png' width='24' height='24' />");
                    console.error("Error 504 Gateway Timeout when accessing  restore.php?file="+ivalue);
                }
            }, success: function(html){            
                // alert("bookmaker in success is \n"+dabookie);
                // console.log("for file"+ivalue+" the html output is "+html );

                
                // console.log(jsonstring); 
                if (html == "success") { 
                    // console.log("#"+ivalue+" successful database import for file "+ivalue);
                    $("tr#filenumber_"+ivalue+" img").attr("src", "tick.png");
                    $("tr#filenumber_"+ivalue).removeClass("redtr");
                    $("tr#filenumber_"+ivalue).addClass("greentr");
                    resolve("success "+ivalue); 
                }
                if (html == "failure" || html != "success") { 
                    console.log("#"+ivalue+" Uh oh! Failed to import into database for file "+ivalue);
                    $("tr#filenumber_"+ivalue+" img").attr("src", "cross.png");
                    $("tr#filenumber_"+ivalue).removeClass("greentr");
                    $("tr#filenumber_"+ivalue).addClass("redtr");


                    addthis = "{ \"goCheckFileNumber\": \""+ivalue+"\" }\n";
                            // alert("addthis is \n"+addthis);
                    addthisDump = appendJSON(addthis, addthisDump);

                    resolve("failure "+ivalue); 
                }


                if (ivalue == 1) { console.log("-----> RESULTS |||||\\/\\/\\/\\/\\/"); }
                
            }, error: function(XMLHttpRequest, status, message){
                console.log("The ajax request error is "+message);
                
                if(message == "timeout"){
                    console.log("timeout error for split file "+ivalue); 
                } 
                
                 else if (XMLHttpRequest.readyState == 4) {
                    // HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText)
                    /*
                    $("."+dabookie+"_horse"+i+" td").addClass("redback");
                    $("."+dabookie+"_horse"+i+" .loading").replaceWith("<img src='cross.png' width='24' height='24' />");
                    */
                    console.log("HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText"); 
                } 
                else if (XMLHttpRequest.readyState == 0) {
                    // Network error (i.e. connection refused, access denied due to CORS, etc.)
                    // $("."+dabookie+"_horse"+i+" td").addClass("magentaback");
                    // $("."+dabookie+"_horse"+i+" .loading").replaceWith("<img src='cross.png' width='24' height='24' />");
                    console.error("Ajax failed. Network error: (i.e. connection refused, access denied due to CORS, etc.\n"+message);
                }
                else {
                    console.error("Error fetching data for unknown reason: \n"+message);
                    //if(message == "Gateway Timeout" || status == "timeout" || status === "timeout") { // stack overflow said so
                    // $("."+dabookie+"_horse"+i+" td").addClass("redback");
                    // $("."+dabookie+"_horse"+i+" .loading").replaceWith("<img src='cross.png' width='24' height='24' />");
                    //}
                }
                resolve("failed "+ivalue);
            }
        });
  }); //close promise
  }


  /////////////////
    // dump JSON in textarea
    //console.log("Stop the next alert executing before the ajax is completed");

    $(document).ajaxStop(function () {
        //alert("hello");
        if (jsondump != ""){
            // alert("FINAL jsondump is \n"+ jsondump);
            //$("textarea").empty();
            realjsondump = `
            { 
                \"failedsplitfiles\": [
                    

                        `+jsondump+`
                    
                ]
            }
            `;            
            $('textarea').html(realjsondump);
            $("#loading").fadeOut(600);
            $(".bulkajaxsuccess").fadeIn(600);
            jsondump = "";
            //$(".togglegreen").fadeIn(300);

            dataString = "savethis="+realjsondump;
            $.ajax({
            type: "POST",
            url: "savejsonfile.php",
            data: dataString,
            cache: false,
            timeout: 12000, //changeme
            success: function(html){
            },
            error: function(html){
                alert("Error saving bulkimportlog.json file.");
            }
            });
        }
    });

}); // end start jquery
    </script>


</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Step 1</a></li>
<li><a>Step 2</a></li>
<li><a>Step 3</a></li>
<li><a>Step 4</a></li>
<li id="active"><a id="current">Step 5</a></li>
<li><a>Step 6</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Petmi v<?=$softwareversion;?></font><br>
<?php
ob_start();
// allows you to use cookies
include ("config.php");

//gets the config page
if (isset($_POST['register']) && $_POST['register'])
{
    if ($dbname == null | $dbprefix == null | $accesshost == null) 
    {
        //checks to make sure no fields were left blank
        echo "A field was left blank.
		<br><a href=\"step2.php\">Go back</a>
		";
    }
    else
    {
        //none were left blank!  We continue...



        
/*
$file = fopen("importme.sql","r");

while(! feof($file))
  {
  echo fgets($file). "<br />
 $chopped = array('one', 2, 'three');
  $chopped [] = 'the fourth element';
  $isit = fgets($file);
$domain = strstr($isit, '-- --------------------------------------------------------');
echo $domain; // prints @example.com
  ";
  }
  */

//echo "$importme";

echo "<hr>";

/*
$importme = "importme.sql";
$importsplit = explode("-- --------------------------------------------------------", $importme);
for($i = 0; $i < count($importsplit); $i++){
	echo "Piece $i = 
	echo "<br>$importsplit[$i] <hr size=\"10\" color=\"#ffcc00\">";
}
echo "<hr size=\"10\" color=\"red\">";


/*

$dbhost = '';
@set_time_limit(0);
$db = mysql_connect($localhost, $username, $password);
if(!$db) die('Cannot connect: ' . mysql_error());
$res = mysql_select_db($database);
if(!$res) die('Cannot select database "' . $database . '": ' . mysql_error());
for($i=0; $i<count($sql); $i++)
{
 if($table_prefix !== 'phpbb_') $sql[$i] = preg_replace('/phpbb_/', $table_prefix, $sql[$i]);
 $res = mysql_query($sql[$i]);
 if(!$res) { echo 'error in query ', ($i + 1), ': ', mysql_error(), '<br />'; }
}
*/
echo ("
<br><br>
<img src=\"tick.png\">The database has been populated.
<br><a href=\"step5.php\">Contiune to step 5</a>
");	
//echo 'done (', count($sql), ' queries).';

		
				
            }
        }
    
	
else
	{
	
		// the form has not been submitted...so now we display it.
		
function ByteSize($bytes) 
    {
      // echo "$bytes";
    $size = $bytes / 1024;
    if($size < 1024)
        {
        $size = number_format($size, 2);
        $size .= ' KB';
        } 
    else 
        {
        if($size / 1024 < 1024) 
            {
            $size = number_format($size / 1024, 2);
            $size .= ' MB';
            } 
        else if ($size / 1024 / 1024 < 1024)  
            {
            $size = number_format($size / 1024 / 1024, 2);
            $size .= ' GB';
            } 
        }
    return $size;
    }		
		
		
		//path to directory to scan
//path to directory to scan
$directory = "splits/";
 
//get all sql dump files with a .sql extension.
$sqldumps = glob("" . $directory . "*.sql");
natsort($sqldumps);

// print_r($sqldumps); exit();
 
//print each file name
echo "
You can click the table rows on and off so you remember which .sql files you've imported.
<br>0.00KB does not represent an empty file. It just means that it's below 1024 bytes.
<br>You can click the table rows with the files to mark the row red, to mark the .sql files that have completed importing.
<br>Once you've tried importing all the files, if there's ones that won't import, import it with phpMyAdmin.
<br>
<br>Or you can click the import all button to import all the split files at once.
<br>
<br>If you're having trouble with this bulk import, adjust the timeout value below for between each import, then see if your server's  (or windows firewall) is blocking this script (or webserver) for having heavy usage within 3 seconds. The minimum optimum value is already filled in for you. The timings aren't perfect due to technical limitations in web browsers.

<br><br><input id=\"desiredtimeout\" name=\"desiredtimeout\" value=\"170\" class=\"bigtext\" style=\"width: 2.35em;\"> 
    <input name=\"justforshow\" value=\"x100ms\" class=\"bigtext\" style=\"width: 4.85em;\" disabled=\"disabled\">
<br><br><input name=\"bulkimportsplits\" type=\"button\" id=\"bulkimportsplits\" class=\"bigbutton\" value=\"Import multiple .sql splits at once\">

<div id=\"loading\"><img src=\"loading.gif\" width=\"126\" height=\"22\" alt=\"loading animation\" /></div>

<div class=\"bulkajaxsuccess\">
    <img src=\"information.png\" width=\"32\" height=\"32\" align=\"left\" />
    <p>The below file has been written to split/bulkimportlog.json for future reference.</p>
    <textarea class=\"mediumtext\" style=\"font-family: meslo_lg_mregular, Courier New; width: 600px; height: 200px;\" readonly=\"\"></textarea><br>
</div>

<br><br><table cellpadding=\"5\" id=\"datable\">";
foreach($sqldumps as $filename)
{
$filesize = filesize($filename);
$filesize = ByteSize($filesize);
$filenumber = str_replace("splits/sql-split-", "", $filename);
$filenumber = str_replace(".sql", "", $filenumber);
echo "
<tr id=\"filenumber_$filenumber\">
<td valign=\"top\"><img src=\"paper.png\" align=\"left\">$filename</td>
<td valign=\"top\">$filesize</td>
<td valign=\"top\">

<a href='restoreme.php?file=$filename'
onClick=\"window.open('restore.php?filenumber=$filenumber&mode=singleimport',
'RIC','width=600,height=400,resizable=yes,scrollbars=yes');return false;\">

Restore database</td>
</tr>
";
}		
echo "</table>";
		echo ("
		<br><br><br>
		<a href=\"step6.php\">I've finished this step</a>
		</form>");
					
		 

	 }

?>			  
			  



</div>			  
</body>
</html>