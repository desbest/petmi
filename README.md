# Petmi

## About

Phpmyadmin export to mysql import (it also accepts mysql databases generated from mysqldump and is compatible with mariadb)

Phpmyadmin export to mysql import (also supports mariasql) allows people to import mysql/mariasql databases which have exceeded 100MB without having silly errors.

It has been tested with a 1GB database.

How it works, is that it breaks the .sql file into chunks and processes each chunk on its own. The chunks which failed to process are given to you at the end of the process to import manually.

## Installing

Simply upload it to your website. Your website or server must run PHP. Petmi is compatible with PHP 4, PHP 5 and PHP 7.

## Testing

Petmi should be tested with [these mysql sql dumps](https://gitlab.com/desbest/mysql-dumps-to-test-in-petmi)

## Help

If you need help simply [create an issue](https://gitlab.com/desbest/petmi/-/issues/new) on gitlab for this repository or [email me](http://desbest.com/contact)

## Changelog
[View the changelog here](https://gitlab.com/desbest/petmi/-/wikis/Changelog)

## More Information
[Petmi web page](https://desbest.com/projects/petmi/) or check the [gitlab wiki](https://gitlab.com/desbest/petmi/-/wikis/  )

Copyright © 2009-2021 desbest
