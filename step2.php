<?php include("config.php"); ?>
<?php
if ( ! function_exists('isMobileDevice'))
{
  function isMobileDevice(){
      $aMobileUA = array(
          '/iphone/i' => 'iPhone', 
          '/ipod/i' => 'iPod', 
          '/ipad/i' => 'iPad', 
          '/android/i' => 'Android', 
          '/blackberry/i' => 'BlackBerry', 
          '/webos/i' => 'Mobile'
      );

      //Return true if Mobile User Agent is detected
      foreach($aMobileUA as $sMobileKey => $sMobileOS){
          if(preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT'])){
              return true;
          }
      }
      //Otherwise return false..  
      return false;
  }
}
?>
<html>
<head>
<title>Petmi</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<meta name="viewport" content="width=device-width">
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<li><a>Step 1</a></li>
<li id="active"><a id="current">Step 2</a></li>
<li><a>Step 3</a></li>
<li><a>Step 4</a></li>
<li><a>Step 5</a></li>
<li><a>Step 6</a></li>
</ul>
</div>

<div class="offset">
<font class="header">Petmi v<?=$softwareversion;?></font>
<br>			

<?php
if (isMobileDevice() === true){
	echo "
	<img src=\"warning.png\" align=\"left\">
	Petmi is not designed to be used on mobile phones due to the temporary and limited way that smartphones allocate memory or RAM.
	<p>Use a computer instead.</p>
  <p>If you continue on your phone it's at your own risk as importing might fail.</p>
  <hr>
	";
}

	echo "
	
<img src=\"information.png\" align=\"left\">
<h2>Information about configuring your database export dump</h2>
Unticking IF NOT EXISTS will make mysql tables not be processed if they already exist.
<br>However ticking IF NOT EXISTS will update the table with new information from your phpmyadmin export.
<br>Unticking ADD DROP TABLE will drop the table in the database in config.php if it already exists in your phpmyadmin export.
<br>However most people leave both options unticked.<br>

<h2>Common errors you might face with mysql</h2>
<b>How to solve and fix “Invalid default value” MySQL error for dates and timestamp?</b>
<br><a href=\"https://apps.badjoerichards.com/apps/developerhack/how-to-solve-and-fix-invalid-default-value-mysql-error-for-dates-and-timestamp/\">[answer one]</a> <a href=\"https://stackoverflow.com/a/59338498/337306\">[answer two]</a>
<br>
<br><b>Disable ONLY_FULL_GROUP_BY</b>
<br><a href=\"https://stackoverflow.com/a/36033983/337306\">The answer is here.</a>

<br><br><br><a href=\"step3.php\">Continue Importing</a>
	";
 ?>

</div>
</body>
</html>